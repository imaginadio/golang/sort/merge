package merge

import (
	"testing"

	"github.com/go-playground/assert/v2"
)

type testCase struct {
	name     string
	srcArray []int
	expected []int
}

var testTable []testCase = []testCase{
	{
		name:     "OK",
		srcArray: []int{9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
		expected: []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
	},
	{
		name:     "OK",
		srcArray: []int{6, 5, 4, 9, 8, 7, 3, 2, 1, 0},
		expected: []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
	},
}

func TestSortMerge(t *testing.T) {
	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			arr := make([]int, len(tc.srcArray))
			copy(arr, tc.srcArray)
			SortMerge(arr)
			assert.Equal(t, tc.expected, arr)
		})
	}
}

func BenchmarkSortMerge(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, tc := range testTable {
			arr := make([]int, len(tc.srcArray))
			copy(arr, tc.srcArray)
			SortMerge(arr)
		}
	}
}

func TestFastSortMerge(t *testing.T) {
	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			arr := make([]int, len(tc.srcArray))
			copy(arr, tc.srcArray)
			sorted := FastSortMerge(arr)
			assert.Equal(t, tc.expected, sorted)
		})
	}
}

func BenchmarkFastSortMerge(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for _, tc := range testTable {
			arr := make([]int, len(tc.srcArray))
			copy(arr, tc.srcArray)
			FastSortMerge(arr)
		}
	}
}
