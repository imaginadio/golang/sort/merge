package merge

func SortMerge(arr []int) {
	l := len(arr)
	if l > 2 {
		ar1 := arr[:l/2]
		SortMerge(ar1)

		ar2 := arr[l/2:]
		SortMerge(ar2)

		// merge
		tmparr1 := make([]int, len(ar1))
		copy(tmparr1, ar1)

		tmparr2 := make([]int, len(ar2))
		copy(tmparr2, ar2)

		var p1, p2 int
		for i := 0; i < l; i++ {
			var min int
			// first pointer out of range - take second array
			if p1 >= len(tmparr1) {
				min = tmparr2[p2]
				p2++
				// second pointer out of range - take first array
			} else if p2 >= len(tmparr2) {
				min = tmparr1[p1]
				p1++
			} else if tmparr2[p2] > tmparr1[p1] {
				min = tmparr1[p1]
				p1++
			} else {
				min = tmparr2[p2]
				p2++
			}
			arr[i] = min
		}
	} else if l == 2 {
		// sort
		if arr[0] > arr[1] {
			buf := arr[0]
			arr[0] = arr[1]
			arr[1] = buf
		}
	}
}

func FastSortMerge(arr []int) []int {
	l := len(arr)
	if l == 2 {
		if arr[0] > arr[1] {
			arr[0], arr[1] = arr[1], arr[0]
		}
	} else if l > 2 {
		ar1 := arr[:l/2]
		ar2 := arr[l/2:]

		return FastMerge(FastSortMerge(ar1), FastSortMerge(ar2))
	}

	return arr
}

func FastMerge(left []int, right []int) []int {
	result := make([]int, len(left)+len(right))
	var p1, p2, res int
	for i := 0; i < len(result); i++ {
		if p1 >= len(left) {
			res = right[p2]
			p2++
		} else if p2 >= len(right) {
			res = left[p1]
			p1++
		} else if left[p1] < right[p2] {
			res = left[p1]
			p1++
		} else {
			res = right[p2]
			p2++
		}

		result[i] = res
	}

	return result
}
